# GeoScenario: An Open DSL for Autonomous Driving Scenario Representation
Rodrigo Queiroz `rqueiroz@gsd.uwaterloo.ca`,
Thorsten Berger `thorsten.berger@chalmers.se`, Krzysztof Czarnecki `kczarnec@gsd.uwaterloo.ca` 
</br>
*Update: 07-May-2019*
<hr>

GeoScenario is a Domain Specific Language (DSL) for scenario representation to support test cases in simulation. The language covers a variety of elements that compose typical traffic situations that need to be formally declared and executed on self-driving vehicle testing. The language is XML based, simple, extensible, and built on top of Open Street Map (OSM) standard. 

GeoScenario full documentation is available at [https://geoscenario.readthedocs.io](https://geoscenario.readthedocs.io)

If you use GeoScenario, please cite our IV’19 <a href="https://uwaterloo.ca/waterloo-intelligent-systems-engineering-lab/sites/ca.waterloo-intelligent-systems-engineering-lab/files/uploads/files/iv2019_0501_fi.pdf" target="_blank">paper</a>:
```
@INPROCEEDINGS{queiroz19,
    title = {{GeoScenario}: An Open DSL for Autonomous Driving Scenario Representation},
    author = {Rodrigo Queiroz and Thorsten Berger and Krzysztof Czarnecki},
    booktitle={2019 IEEE Intelligent Vehicles Symposium (IV)}, 
    year = {2019}
}
```
<div style="float: center;">
<img  src="docs/img/wiselogo.png" width="100">
<img src="docs/img/uwlogo.png" width="200">
</div>

### Repo structure

- **docs**: documentation.
- **gschecker**: GS Checker is a python script to parse and evaluate a given GeoScenario file, checking its structure and syntax conformity to the language. 
- **josm**: contains a collection of presets and stylesheets to design GeoScenarios in JOSM.
- **lanelets** sample lanelet files to represent the road network.
- **scenarios** sample scenarios in GeoScenario. 

Tool instructions and a discussion for the sample scenarios are available in the [documentation](https://geoscenario.readthedocs.io/en/latest/Tools/)