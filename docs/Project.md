<h1> Project </h1>

![moose](img/moose_sideways.jpg)

GeoScenario was created in the Waterloo Intelligent Systems Engineering Lab [(WISE Lab)](https://uwaterloo.ca/waterloo-intelligent-systems-engineering-lab/) and is part of the Autonomoose Project ([Autonomoose](https://autonomoosedev.wixsite.com/autonomoose/)). 

“Autonomoose” is the University of Waterloo self-driving research platform. The platform is a Lincoln MKZ Hybrid modified to autonomous drive-by-wire operation and a suite of lidar, cameras, inertial and vision sensors. The car is equipped with computers to run a complete autonomous driving system, integrating mapping, sensor fusion, motion planning, and motion control software in a custom autonomy software stack fully developed at Waterloo as part of the research. The system was the first Canadian-built ADS to be tested on public roads in Canada in August 2018.

The autonomy stack is implemented on top of Robot Operating System (ROS) framework and tested in a custom simulation tool-set developed with Unreal Engine 4. We use GeoScenario in the simulation infrastructure as the official format for all test case scenarios.

<h3> What is next? </h3>

We released the first version of GeoScenario. The language will evolve with feedback from the community and will be constantly updated to increase its expressiveness.
More tools from WISE Lab will be available to facilitate its application in other projects, and more open scenarios will be distributed. 

Our team is working on a benchmark for motion planning with more than 100 scenarios designed with GeoScenario format. All scenarios will be open and freely available ([WISE Bench website](http://wiselab.uwaterloo.ca/wisebench/)). Additionally, a database of critical scenarios based on pre-crash and crash traffic data will be distributed in GeoScenario format. 

<h3> GeoScenario contributors: </h3>

- Rodrigo Queiroz  <rqueiroz@gsd.uwaterloo.ca>
- Thorsten Berger  <thorsten.berger@chalmers.se>
- Krzysztof Czarnecki  <kczarnec@gsd.uwaterloo.ca>
- Michal Antkiewicz  <mantkiew@gsd.uwaterloo.ca>
- Marko Ilievski  <marko.ilievski@uwaterloo.ca>
- Michael Ala  <michael.ala@uwaterloo.ca>
- Evan Kim  <evan.kim@uwaterloo.ca>
- Kapilan Satkunanathan  <kapilan.satkunanathan@uwaterloo.ca>


We are happy to accept contributions from the community by reporting bugs, ideas for new features, code and tool contributions, or sharing scenarios built with GeoScenario. Use the issues section on our [git project](https://git.uwaterloo.ca/wise-lab/geoscenario/issues) or send us an email: <rqueiroz@gsd.uwaterloo.ca>.
