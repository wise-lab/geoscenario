<h1> Traffic Lights </h1>

A traffic light physical object is part of the scenery an is defined in the road network (lanelet), but since it's state is dynamic it must also be defined in GeoScenario file. For exmaple, the standard `green, yellow,red` states. Note that different countries can have different states and different sequences. Additional states can also be added (e.g., turn, pedestrian walk, etc). The time per each state is given in (s) as a list with the 'duration'attribute'. Traffic light states can also be changed by trigger (see [Orchestration](Orchestration.md) section).

GeoScenario traffic light:

```
<node id=’-1’ lat=’43.5094’ lon=’-80.5367’>
    <tag k=’gs’ v=’trafficlight’ />
    <tag k=’name’ v=’intersection_1_light’ />
    <tag k=states v=’green,yellow,red’ />
    <tag k=’duration’ v=’15,3,20’ />
</node> 
```

Traffic light attributes:

k | v | description
-|-|-
gs* | 'trafficlight' | GS role key
name* | string | A name for the Traffic Light
states | string list | list of state names. Example: `green, yellow,red`. 
duration | time (s) | List of time per state. Must follow the same state order.