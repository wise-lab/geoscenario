<h1>Tools</h1>

##GeoScenario Editor (custom JOSM)
Since our format was developed on top of OSM primitives, we adapted it’s standard map editing tool: JOSM . By adding a set of custom presets and style sheets, we can now easily design and understand a Geoscenario on top of the Road Network (Lanelet layer) or other map layers (e.g., Bing Maps, ESRI maps) before its execution. 

All presets are availabe in our repo [https://github.com/rodrigoqueiroz/geoscenario](https://github.com/rodrigoqueiroz/geoscenario).

How to install:

- Download JOSM  at [https://josm.openstreetmap.de/](https://josm.openstreetmap.de/)
- Open the configuration dialog (press F12), then Settings for the map projection:
- In "Tagging Presets" tab, add Geoscenario file "josm/geoscenario_presets_version.xml" to the list of Active Presets.
- In "Map Paint Styles" tab, add Geoscenario file "josm/geoscenario_mapstyle_version" to the list of Active Styles.
- In both tabs, go to "Icon paths" and add the Geoscenario icons directory "josm/icons"
- To easily acess Goescenario presets, go to "Customize the elements in the tool bar" and add Presets>GeoScenario Tools to the list. JOSM must be restarted in order to apply the changes.

![custom josm](img/josmintersection.png)

Warning: GeoScenario files are not part of the OSM. Never upload GeoScenario files as maps to the official OSM data base. If you want to have an OSM server to host your maps, , you can set up your own server.

##GeoScenario Checker
A set of scripts is available to evaluate a Scenario’s conformity with the standard language features. 
Currently the tool provides only syntax checking. We intend to evolve this tool with the ability to preview and detect unfeasible scenarios.
The tool is availavle in our repo: [https://github.com/rodrigoqueiroz/geoscenario](https://github.com/rodrigoqueiroz/geoscenario).

##Coming soon
A complete Driving Scenario Simulator with full support to GeoScenario will be released as open source Unreal Plugin, supporting a series of available open source Unreal based simulation tools (e.g., AirSim, Carla ). We also plan to publish a shared database of tool-independent test scenarios for self-driving vehicles.


##External Tools
####liblanelet
Is a C++ open source tool to parse OSM XML files and build lanelet maps, provinding access to its structure and routing.
We recommend visiting their repo and checking the Lanelet Paper
[https://github.com/phbender/liblanelet](https://github.com/phbender/liblanelet)

####OpenDRIVE to Lanelet converter
If you have maps in OpenDRIVE format (www.opendrive.org), you can convert them to Lanelet using this tool provided by researchers from CommonRoad. </br> 
[https://pypi.org/project/opendrive2lanelet/](https://pypi.org/project/opendrive2lanelet/)

####GeographicLib

Open C++ library for performing conversions between geographic, UTM, UPS, MGRS, geocentric, and local cartesian coordinates. We recommend geographiclib to convert coordinates between GeoScenario and Lanelet maps to your simulation environment coordinate system.
[https://geographiclib.sourceforge.io/](https://geographiclib.sourceforge.io/)