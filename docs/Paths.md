<h1> Paths </h1>

![paths](img/path.png)

In order to move, vehicles and pedestrians need to be assigned to a path. A path is defined as a way element, and can be used for both vehicles and pedestrians. Paths should be interpreted as splines composed by ordered connected nodes. When a dynamic agent is assigned to a path, it will travel along the path with its reference speed.
To support more realistic kinetics with variable velocity and acceleration, or to reproduce scenarios from recorded traffic data, an agent can be assigned to a speed profile. When a path has a speed profile, it must contain nodes with the tag agentspeed to indicate the target speed in km/h for the agent once it reaches that node. The agent must always try to match the speed of the next node in its path with a constant acceleration.

With high density paths (i.e., more nodes) and a speed profile, a GeoScenario model can represent a diverse range of traffic situations, manually designed by experts, extracted from real traffic by sensors, or imported from naturalistic driving databases. In the following example, note how the ID references to the nodes composing a path are given by the tag 'nd' and must be interpreted as an ordered list.

GeoScenario path example:
```xml
<way id='-166757'>
    <tag k='gs' v='path' />
    <tag k='name' v='east_path' />
    <tag k='abstract' v='no' />
    <nd ref='-133584' />
    <nd ref='-133598' />
    <nd ref='-133600' />
    <nd ref='-133604' />
</way>
```
Speed profile node example:

```xml
<node id='-133584' lat='43.50929904003' lon='-80.53401271512'>
      <tag k='agentspeed' v='30' />
      <tag k='agentacceleration' v='1' />
      <tag k='timetoacceleration' v='0.4' />
</node>
```

By default, all paths are grounded to fixed node coordinates. We introduce the tag abstract to define flexible paths. Abstract paths are designed on fixed coordinates, but during execution must be shifted to a new origin point based on the agent’s current location. Abstract paths can be used to design dynamic maneuvers. For example, a lane change that can occur at different locations of the road network.

Path attributes:

k           | v         | description
----------- | ----------| ----------------------
gs*         | paths     | GS role key
name*       | string    | A name for the Agent
abstract    | bool      | If the path as abstract, the points must be adapteed to the agent's current location.
nd          | Node id list | Unlike other attributes, the node references are defined using a list of <nd/> tags and must be interpreted in the given order. <br> Example: ```<nd ref='-1' /> <nd ref='-2' />```

Speed profile node attributes:

k                   | v                     | description
--------------------|-----------------------| ----------------------
agentspeed          | float (km/h)          | the velocity that the agent should have when it reaches this node.
agentacceleration   | float (m/s<suo>2</sup>)             | the acceleration the agent should use to achieve the velocity at the next node.
timetoacceleration  | time (s)              | the time in which an agent should reach the given acceleration. The acceleration is linearly increased from the agent's previous acceleration to the target one over this period of time.