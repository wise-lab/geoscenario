<h1> Static Objects</h1> 

To represent stationary obstacles that are not part of the Road Network but blocks or limits the drivable surface, we introduce the element of type gs = staticobject, based on a single node, a way, or a closed-way. A closed-way can assume arbitrary shapes, but in order to be valid, it must have the first and last node reference pointing to the same node id. A reference to a model can be used to give the object a more defined form. We chose to keep the Geoscenario simple and flexible, and the model must be defined elsewhere.
```
<node id='-133668' lat='43.51019007127' lon='-80.53566190827'>
    <tag k='gs' v='staticobject' />
    <tag k='area' v='no' />
    <tag k='name' v='obstacle2' />
  </node>
```

k           | v                 | description
------------|-----------        |-------------
gs*         | 'staticobject'    | GS role key
name*       | string            | A name for this object.
orientation | int (deg)         | Object's orientation in degrees
model       | string            | Reference to a model to be used in simulation. External details of the model should be provided with GeoScenario.
height      | float (cm)        | The object's height in cm.
area        | bool              | When defined as a way element, the object can be interpreted in two forms: (i) *area=no*: the shape only indicates the object boundaries while keeping its original dimensions (maximum width and length). However, the object still keeps its natural shape. Example: a garbage bin. (ii) *area=yes*: this object must be interpreted as a polygon following the same shape of the polygon (this is only valid if the last and first nodes of the way are the same. Ex: snow piles accumulating at the side of the road and blocking the drivable area. When static object is defined as a node, this attribute is always false ('no'). Dimensional must be provided by the object model, or by adding new custom attribute (*external=yes*).

