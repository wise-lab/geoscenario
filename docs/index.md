<h1> GeoScenario: An Open DSL for Autonomous Driving Scenario Representation </h1>
Rodrigo Queiroz <rqueiroz@gsd.uwaterloo.ca>,
Thorsten Berger <thorsten.berger@chalmers.se>, Krzysztof Czarnecki <kczarnec@gsd.uwaterloo.ca>
</br>
*Update: 08-May-2019*
<hr>

GeoScenario is a Domain Specific Language (DSL) for scenario representation to support test cases in simulation. The language covers a variety of elements that compose typical traffic situations that need to be formally declared and executed on self-driving vehicle testing. The language is XML based, simple, extensible, and built on top of Open Street Map (OSM) standard. 

This documentation website provides a detailed specification of the language and all its components, including examples and usage instructions. Tools and XML files for all scenarios mentioned in this document are available in the project's repository at 
[https://git.uwaterloo.ca/wise-lab/geoscenario](https://git.uwaterloo.ca/wise-lab/geoscenario)


<h3>Article</h3>
If you use GeoScenario, please cite our IV’19 <a href="https://uwaterloo.ca/waterloo-intelligent-systems-engineering-lab/sites/ca.waterloo-intelligent-systems-engineering-lab/files/uploads/files/iv2019_0501_fi.pdf" target="_blank">paper</a>:
```xml
@INPROCEEDINGS{queiroz19,
    title = {{GeoScenario}: An Open DSL for Autonomous Driving Scenario Representation},
    author = {Rodrigo Queiroz and Thorsten Berger and Krzysztof Czarnecki},
    booktitle={2019 IEEE Intelligent Vehicles Symposium (IV)}, 
    year = {2019}
}
```
<div style="float: center;">
<img  src="img/wiselogo.png" width="100">
<img src="img/uwlogo.png" width="200">
</div>
