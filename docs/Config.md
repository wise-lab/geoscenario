<h1> Scenario Configuration </h1>

<h2> Global Config </h2>
Global configurations for a scenario. Defines a name for a scenario, the road network (lanelet), failure conditions (timeout or collision) and other global attributes.

```xml
<node id='-86'>
    <tag k='gs' v='globalconfig' />
    <tag k='version' v='1.1' />
    <tag k='name' v='Sample Scenario' />
    <tag k='author' v='rqueiroz@uwaterloo.ca' />
    <tag k='notes' v='sample geoscenario config node' />
    <tag k='lanelet' v='straight_road.osm' />
    <tag k='collision' v='yes' />
    <tag k='timeout' v='65' />
    <tag k='metric' v='ttc_ego_v1' />
  </node>
```

Global Config attributes:

k           | v                 | description
------------|-------------------|-------------
gs          | 'globalconfig'    | GS role key
version*    | string            | GeoScenario version. Current version is 1.1
name*       | string            | A name for the scenario
author      | string            | Scenario autorship
notes       | string            | Any additional notes to support readers
lanelet*    | filename          | Lanelet file representing the Road Network
collision*  | boolean           | If true, a scenario will fail when a collision happens with Ego.
timeout*    | time (s)          | If true, a scenario will fail when reaching the given time.
metric      | name [l]          | Metrics that must to be collected in this scenario. Supports lists. Ex: "ttc_ego_v1, ttc_ego_v2"


<h2> Origin </h2>

Origin node is an arbitrary reference point in space to align scenario, map, and simulation environment. Scenario and road network files designed on top of existent roads with global coordinates are naturally aligned. However, to run test scenarios in a virtual simulation environment, a reference to translate scenario coordinates (lat,lon) to the virtual coordinate system (typically cartesian) is necessary. 
Technically, any node in the GeoScenario with coordinates can be used as the origin. But having an independent element for this role makes it explicit, and easier to match the same location in the simulation environment.
For example, an intersection scenario can have its origin point in the middle of the intersection. This point is assumed to be the same middle intersection point in the simulation environment coordinate system (x,y,z), and all remaining scenario coordinates can be converted by distance to this reference point in a 3D space. 
Users are free to use any conversion method of preference. Keep in mind that any simulation feedback to the ADS driving Ego must use the same conversion method to ensure simulated sensors and all scenario elements will align. We also recommend selecting a point that is not too far from the scenario nodes to reduce acumulated error from the conversion method. 

Example: 

```xml
<node id='-16' visible='true' lat='43.50985347737' lon='-80.53770874162'>
    <tag k='gs' v='origin' />
</node>
```

This node does not have additional attributes. 

k           | v                 | description
------------|-------------------|-------------
gs          | 'origin'          | GS role key

<h2> Metric </h2>
A metric is also defined as an element in GeoScenario, by explicitly declaring which agents are tracked. We encourage scenarios to include references for how a metric is calculated since different approaches can be used. For example, TTC can be computed using a variety of methods leading to different values. 

A GeoScenario metric:

```xml
<node id='-6'>
    <tag k='gs' v='metric' />
    <tag k='name' v='ttc_ego_vehicle1' />
    <tag k='reference' v='TTC' />
    <tag k='agents' v='Ego,vehicle1' />
</node>
```

Metric attributes:

k           | v             | description
------------|---------------| -----------
gs*         | 'metric'      | GS role key
name*       | string        | ID name for the metric
reference   | string        | Reference to the metric. We strongly suggest including an external reference with details of how the metric is calculated.
agents      | name [l]      | Agents to track. Usually, metrics are calculated for a single agent (e.g., tracking energy, progress, or comfort), or between two agents (distance, range, gap, TTC). Multiple agents can be added as a list of names `agents='agent1, agent2'`.</br>Note how the order is important (TTC A->B is different from TTC B->A). </br>Example: `agents='Ego,vehicle1'`.

Note: *globalconfig* and *metric* nodes coordinates have no relevance to the scenario and can be omitted. Howerver, if you use JOSM to create a scenario they will be added to the node regardless.