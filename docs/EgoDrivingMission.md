<h1>Ego and the Driving Mission</h1>

![ego driving mission](img/egodrivingmission.png)

In a Scenario, Ego is the entity representing the ADS. In our language we do not define actions or maneuvers for the Ego. Instead, GeoScenario only specifies initial conditions and goals. During a Test Case execution time, the ADS is a black box system responsible for deciding the best route and maneuvers based on the traffic conditions (road network, static objects, dynamic agents on the path, etc.). 
We decided for this approach to allow the language to be system independent and to reflect a real world driving scenario. In practice, a Driving Mission is given to the Driver or ADS as a global location to be reached as a long-term task. The initial condition is defined as a Node representing Ego starting position and orientation. We assume Ego always starts a scenario in a parked position.

```xml
<node id='-133664' action='modify' visible='true' lat='43.50935005775' lon='-80.53852421809'>
    <tag k='gs' v='egostart' />
    <tag k='orientation' v='0' />
  </node>
```

Ego Start attributes:


k            | v          | description
-------------|------------|-----------
gs*          | 'egostart' | GS role key
orientation* | int (deg)  | Ego starting orientation in degrees.

The Goal is defined as an egogoal node. A scenario can have multiple goal locations. They can be used to define partial goals and must be ordered to compose a Driving Mission with multiple steps. The final goal for the Driving Mission Task is the one with highest order number and must finish the scenario with a success state. The nodes can be used to compose a global path for the system, or create a goal point on the system’s internal map. However, this is particular to the ADS configuration and is out of the scope of our model. If an order is not given, any unordered goal is enough condition for termination of a scenario with success state.

```xml
<node id='-133666' lat='43.50934795086' lon='-80.5360889789'>
    <tag k='gs' v='egogoal' />
    <tag k='name' v='maingoal' />
    <tag k='order' v='1' />
  </node>
```
k           | v         | description
------------|-----------|-------------
gs*         | 'egogoal' | GS role key
name*       | string    | A name for this goal.
order       | int       | Order (increasing) for this goal. Used when multiple goals must be reached in a certain order. Any unordered goal is interpreted as sufficient condition for termination of a scenario with success state. If order and unordered are available, a scenario must success if at least one unordered goal is reached, or all ordered goals are reached in the right order.
