<h1> GeoScenario Architecture </h1> 

<h2> Elements Overview </h2>
Elements with a role (i.e., with a *gs* key tag) are the main components of a GeoScenario. The following table shows a summary of all components supported in the latest version. Note that a component can be created with more than one element type (node, way or closed-way).
The icons are not part of the language, but are used in this document as a reference to the GeoScenario [Editor](Tools.md) (custom JOSM)). Each component will be detailed in the next sections.


Icon | GS Key   | Type      | Properties    | Description
-----|----------|-----------|---------------|---------------
![](img/icons/config.png)   | globalconfig    | node      | unique, mandatory | Global configuration for a scenario. Defines a name for a scenario, the road network, and global failure conditions.
![](img/icons/origin.png)   | origin          | node      | unique, optional  | Reference point in space (0,0) to align coordinates with different layers in simulation.
![s](img/icons/metrics.png) | metric        | node      | multiple, optional | Metric. Defines a metric that needs to be tracked during the scenario.
![](img/icons/vehicle_ego.png) | egostart      | node      | unique, mandatory | The Ego starting position at the beginning of a scenario.
![](img/icons/egogoal.png) | egogoal       | node      | multiple, mandatory | Goal position for Ego. Reaching this point is the standard success criterion for a scenario (alternative criteria is also possible using triggers). Optionally, can be defined as multiple points to be reached in sequence.
![goal](img/icons/staticobject.png) |staticobject   | node, way, closed-way | multiple, optional | Static Objects used to define obstacles blocking the drivable surface or impairing sensor detection.
![goal](img/icons/vehicle.png) | vehicle        | node      | multiple, optional | Vehicles as Traffic Agents that can move during a scenario. Includes vehicles from all sizes and types (car, bicycle, truck, bus, etc)
![goal](img/icons/pedestrian.png)   | pedestrian    | node      | multiple, optional | Pedestrians as Traffic Agents that can move during a scenario.
![goal](img/icons/path.png)         | path          | way       | multiple, optional | Paths describing trajectories for both Pedestrians and Vehicles.
![goal](img/icons/location.png)     | location      | node, way, closed-way | multiple, optional |Named location, used as a reference point for other elements within a scenario. For example, an area where a pedestrian will be placed.
![goal](img/icons/trigger_red.png) | trigger       | node      | multiple, optional | Trigger used to orchestrate a scenario with actions for dynamic elements. A trigger can be executed by reaching its location, after a given time, or when a condition is true.
![goal](img/icons/trafficlight.png) | trafficlight  | node      | multiple, optional | Traffic light states (the physical traffic light is defined as a stationary element and is part of the road network)

The figure shows a meta-model of our main GeoScenario components in a Class Diagram notation from UML. 

![orientation](img/diagram.png)

In the next sections we will describe how all those elements work and interact. A table with attributes for each element will be provided. The following symbols will be used: 

```html
[l]   // support lists 
[r]   // support range
[d]   // support distributions
*     // mandatory attribute (all other are assumed to be optional)
(x)   // unit of measurement. If not provided, it follows GeoScenario standard units.
```


