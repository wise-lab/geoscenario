<h1> GeoScenario Architecture </h1> 

<h2>Basic Structrure </h2> 
A GeoScenario document is a valid OSM XML document (filename.osm), using the following structure, and containing all supported elements that compose a GeoScenario and references to external elements (e.g., the road network):

```xml
<?xml version='1.0' encoding='UTF-8'?>
<osm version='0.6'>
    <node ... />
    <way ... />
    ...
</osm>
```

Geoscenario elements are based on two OSM primitive types: Node and Way. 

**Nodes** are the core elements of Geoscenario, representing a specific point on Earth’s surface. Each node comprises an ID number and a pair of coordinates (latitude and longitude). Nodes are used to define standalone point features (e.g., a Vehicle or a Pedestrian), but also to compose the shape of other elements (e.g. a path). 

**Ways** are ordered lists of Nodes defining a polyline. Ways are used to define linear features such as Paths and boundaries of areas (solid polygons that represent an obstacle on the road, or a named area for dynamic element placement). In order to define areas, the way’s first and last node must be the same (in OSM, they are called closed-way).

Example:

![orientation](img/gs_basics.png)

```xml
<node id=1 lat='43.50934231789' lon='-80.53670525588' />
```

```xml
<way id='10'>
    <nd ref='2' />
    <nd ref='3' />
    <nd ref='4' />
</way>
```

```xml
<way id='20'>
    <nd ref='5' />
    <nd ref='6' />
    <nd ref='7' />
    <nd ref='5' />
</way>
```

All elements (nodes and ways) can have tags describing attributes of an element with the pair of text fields **k** (key) and **v** (value). We use a tag **gs** to define an element’s role in the scenario. That is, the element’s function in the GeoScenario model (e.g., gs = vehicle). Elements without a gs tag do not have a specific role in the scenario, but can be used to compose other elements. For example, nodes composing a Path do not have a gs tag. 

Sample GeoScenario vehicle node:

```xml
<node id=’-1’ lat=’43.5094’ lon=’-80.5367’>
   <tag k=’gs’ v=’vehicle’/>
   <tag k=’name’ v=’leading_vehicle’/>
</node>
```

<h4> Element core rules </h4> 

- An element cannot have 2 tags with the same key.
- All nodes have coordinates in WGS84 coordinate frame (as part of the OSM standard), defined by *lat* and *lon* attributes. 
- All elements must have unique integer ID. Different types have their own ID space (a node id=1 and a non-related way with id=1 can co-exist).
- Negative IDs indicate local elements valid only in a single GeoScenario file. When a file is uploaded to the server (optional) a unique global positive ID is assigned to all elements.
- Elements with a role must also contain the tag ’name’ (with a few exceptions). The name is a unique string [a-z][0-9] (case insensitive) that identifies one element is our scenario. This tag is used to derive relations between elements. 
- External attributes: Custom tag attributes added to a scenario must additionally contain  `external='yes'` added tothe tag. This tag ensures the standard validation tools will bypass this attribute.

<h4> Attributes with multiple values </h4>

- Attributes supporting multiple values are described with comma-separated list ("a,b,c") or ranges ("min:max"). Composition of lists (or ranges) inside another list is also supported with '[]'. How the values must be interpreted depends on the element itself. <br>
Examples: ``speed="10,20,30"``, ``speed="10:30”``, ``value="[10:20],[30,31,32]”``. 
- When a list of scenario element names is expected (for example, a list of vehicle names), the following keywords (case insensitive) can be used for simplicity: 'vehicles' for all vehicles in the scenario, 'pedestrians' for all pedestrians in the scenario, 'agents' for vehicles and pedestrians, and '*' for all supported elements in the scenario (including Ego). These keywords are protected and can not be used to name GeoScenario elements of any type; 
- Stochastic distributions are defined in the format "dist.name(param1=v1,param2=v2, ...)". <br>
Example: ``speed="dist.normal(mean=5.0, sd=2, size=100)"``.<br> 
Since many distributions can be used (with diverse parameter names), they must be defined in detail elsewhere. We encourage users to always provide a reference to ensure reproducibility between tools.

example             | how to interpret
--------------------| --------------------
`1, 2, 3`           | list with integer values
`[1,2,3]`           | same as above
`v1, v2, v3`        | list with 3 elements (by name reference)
`1 : 3`             | integer range from 1 to 3
`0.0 : 1.0`         | float range from 0 to 1.0
`1:3,7:9`           | list with 2 elements: integer range from 1 to 3, and integer range from 7 to 9
`[1,2,3],[6,7,8]`   | list with 2 elements: integer list, and second integer list
`agents`            | list with all dynamic agents in the scenario (by name reference)
`vehicles`          | list with all vehicles in the scenario (by name reference)


<h4> Standard Units </h4> 

If not stated otherwise, GeoScenario attributes use the following units:

measure | units
- | -
lat, lon | coordinates in WGS84 frame
speed | kilometers per hour (km/h)
acceleration |  in meters per squared second (m/s<sup>2</sup>) 
time | seconds (s)
size, distance | centimeters (cm) 
boolean | "yes" / "no" (case insensitive). Additionally, "on","true" / "off", "false" are supported.
orientation | in degrees, clockwise, with origin on east. See figure.

Orientation:

![orientation](img/gs_orientation.png)
 
 
