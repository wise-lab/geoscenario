<h1> Vehicles and Pedestrians </h1>

![vehicles pedestrians](img/vehiclespedestrians.png)

We define as Dynamic Elements all Geoscenario elements that are able to move (having kinetic energy) or are able to change their state. This includes vehicles, pedestrians, weather and regulatory element states. Even if they remain unchanged during the scenario, they are still dynamic elements (e.g., a parked vehicle). 

Dynamic Elements that are able to move are called Agents, and are separated in two types: Vehicles and Pedestrians. Both are represented as Nodes and share similar attribute tags. Vehicle is defined with the tag gs = vehicle, and pedestrian with tag gs = pedestrian. The orientation tag is used do define an Agent’s initial orientation (for example, a vehicle yaw). Different types of vehicles (e.g., car, truck, bus) are represented with the same type, with an optional attribute model specifying a vehicle model. We don’t specify details of the vehicle model dynamics or 3D meshes. Therefore, testing results must take into account additional details of the simulation infrastructure running the scenario. A speed attribute (in km/h) is used to define a standard constant velocity. More complex kinetics with variable velocity and acceleration is defined in the Path element.
				
```					
<node id=’-1’ lat=’43.5094’ lon=’-80.5367’>
   <tag k=’gs’ v=’vehicle’ />
   <tag k=’name’ v=’leading_vehicle’ />
   <tag k=’speed’ v=’30’ />		
  <tag k=’orientation’ v=’45’ />
   <tag k=’path’ v=’northpath’ />
   <tag k=’usespeedprofile’ v=’yes’ />
</node> 
```


Vehicle attributes:

k | v | description
- | - | -----------
gs* | vehicle | GS role key
name*   | string    | A name for the Agent
speed   | float (km/h) | Vehicle standard target speed
orientation | int (deg) | Vehicle starting orientarion
cycles | int | Number of times the vehicle will follow a given path. If not given, the vehicle must follow the path once. If -1, the vehicle will loop through the path untill scenario ends. This attribute can be used to simulate dense traffic situations without creating every single vehicle as an individual element.
usespeedprofile | bool (yes/no) | If yes, when assigned to a path a vehicle will follow the speed profile from the path. Otherwise, the vehicle will follow the path with its standard target speed.
model   | string | A reference to a vehicle model to be used in simulation


Pedestrian attributes:

k | v | description
- | - | -----------
gs* | pedestrian | GS role key
name* | string | A name for the Agent
speed | float (km/h) | Pedestrian standard target speed
orientation | int (deg) | Pedestrian standard orientation
usespeedprofile | bool (yes/no) | If yes, when assigned to a path a pedestrian will follow the speed profile from the path. Otherwise, the pedestrian will follow the path with its standard target speed.
model   | string | A reference to a pedestrian model to be used in simulation
