<h1>Scenario Orchestration: Triggers & Actions </h1>

![paths](img/trigger.png)

In GeoScenario we introduce triggers & actions to orchestrate how a scenario evolves. The basic concept is to add trigger nodes in strategic places of the road network, and activate different actions over dynamic elements. Each triggers has owners and targets. Owners activate triggers, whereas targets execute the action (Figure 5). Owners can be the Ego itself or agents (vehicles, pedestrians). Targets can be any dynamic element whose state can change over the scenario, but can not be Ego. This rule follows our assumption of the ADS as a black box system, limited to the initial conditions and the driving mission. Actions can change an agent’s state, or the scenario itself.

![trigger](img/trigger2.png)

A trigger can be activated by three types of conditions, or by a combination of them: 

- *(i) Time:* activated when the scenario execution reaches a given time t. A set of timed triggers allow the designer to control the scenario in chronological order with timed events. For example, at a given time `t = 10`, a pedestrian starts crossing an intersection. 
- *(ii) Location:* activated by overlap, when the owner reaches the trigger node location. Must be placed over strategic points of the Road Network. They are especially useful when timed events can not guarantee Ego and other agents are at the right place at the right moment. For example, one can place a trigger with `owner = Ego`, and and action for a pedestrian to start a path over a crosswalk. This trigger guarantees the walking happens at the desired distance between Ego and pedestrian. 
- *(iii) Metric condition:* activated when a given condition based on a metric is true. This trigger allows situations where an Action needs to be performed with no specific location, but at any location after a relative condition. For example, a vehicle moving over a path on the road starts to decelerate to stop only when the distance between Ego and a vehicle is less than 100 meters. To support a condition, a GeoScenario needs to track a given metric between agents. 

When activated, a trigger can perform the following actions: 

- *Start an agent*: `action = 'astart'`. Target starts its defined behaviour (for example, follow a path). 
- *Set speed*: `action = 'aspeed':` Target set current speed over its path. Overrides current speed or speed profile.
- *Set speed profile*: `action = aspeedprofile` Target start (or stop) following a path's speed profile.
- *Set path*: Target follows a new path.
- *Change location*: `action = 'alocation'` Target is placed in a new location (instant).
- *Change state*: `action = astate` Target change its current state. Can be used for any element with state attribute (e.g., Traffic Light).
- *Fail scenario*: `action = afail` Ends Scenario with a fail state.
- *Success scenario*: `action = asuccess` Ends Scenario with a success state.

One or multiple actions can be performed by the same trigger. If multiple actions are defined, they must be started in sequence they are declared, but execution is independent (paralell). 


A GeoScenario trigger:

```
<node id=’4’ lat=’43.50909’ lon=’-80.53654’>
   <tag k=’gs’ v=’trigger’ />
   <tag k=’activate’ v=’location’ />
   <tag k=’name’ v=’start_trigger’ />
   <tag k=’owner’ v=’Ego’ />
   <tag k=’target’ v=’leading_vehicle’ />
   <tag k=’apath’ v=’west_path’ />
   <tag k=’aspeedprofile’ v=’yes’ />
</node>
```

Trigger attributes:

k               | v                 | description
--------------- | ----------------- | ------------------------------------------------------------------
gs*             | 'trigger'         | GS role key
name*           | string            | A name for the trigger
radius          | float (cm)        | Defines a location based trigger. This value gives the trigger activation radius from the node as the central point.
time            | float (s)         | Defines a timed trigger. Action is executed when scenario reaches the given *time* in seconds.
metric          | string/list       | Defines a metric condition trigger. Must contain the metric name defined in a metric node (or multiple metric names as a list). Action is executed when the given metric name reaches the value range from *value* field.
value           | multiple [lr]     | Metric value range to activate the trigger. Can be listed as single value (`value=100`), a list (`value=50,100,200`), or a range (`value=50:200`). Multiple metrics can have their respective activation values as lists following the same metric order from the  *metric* attribute. Note that lists inside lists must be grouped with '()'. Example: `value=(50,100), 3.4` 
owner           | string [l]        | Agent that activates a trigger. Owners can be the Ego itself, other agents (vehicles, pedestrians), or 'global' (the scenario is the owner when trigger is timed). Can be declared with multiple owners as a list, or using the following keywords to group targets: 'agents', 'vehicles', 'pedestrians', '*' (for any element, including Ego). Note this attribute is only used when the trigger is location based. Timed or condition only triggers are implicitly owned by the scenario itself (global).
target          | string [l]        | Target names that execute an action once the trigger is activated. Targets can be any dynamic element whose state can change over the scenario (but can not be Ego).
aspeed          | float [lrd]       | Action: target's new speed after activation.
aspeedprofile   | bool              | Action: target starts or stops following a path's speed profile.
alocation       | string            | Action: target moves to the given location.
apath           | string            | Action: target starts following a path.
astate          | string            | Action: target change state. Target can be any element with state attribute (e.g., Traffic Light).
astart          | string            | Action: target starts its defined behaviour. Agents need to have startinmotion property set as false, otherwise they do not wait for this action event.
afail           | bool              | Action: target is global. Ends Scenario with a fail state.
asuccess        | bool              | Action: target is global. Ends Scenario with a success state.
delay           | float (s)         | Time (in seconds) between the trigger activation and the action execution. Can be used to delay actions.

